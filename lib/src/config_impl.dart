// Copyright (c) 2015, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library config.config.impl;

import 'package:config/src/config.dart';
import 'package:constrain/constrain.dart';

typedef C EnvironmentConfigs<C extends Config>(String environment);

class ConfigFactoryImpl<C extends Config> implements ConfigFactory<C> {
  final C _common;
  final EnvironmentConfigs<C> _environmentConfigs;
  final Validator _validator = new Validator();

  ConfigFactoryImpl.standard(C common, {C development(), C production()})
      : this.custom(common, (String environment) {
          switch (environment) {
            case StandardEnvironmentNames.development:
              return development == null ? null : development();
            case StandardEnvironmentNames.production:
              return production == null ? null : production();
            default:
              throw new ArgumentError(
                  'unsupported environment value $environment');
          }
        });

  ConfigFactoryImpl.custom(this._common, this._environmentConfigs);

  @override
  C configFor(String environmentName,
      {bool throwIfNoEnvironmentConfig: false}) {
    final environmentConfig = _environmentConfigs(environmentName);
    if (environmentConfig == null && throwIfNoEnvironmentConfig) {
      throw new ArgumentError.value(
          'No config for environment named $environmentName');
    }
    final config =
        environmentConfig != null ? _common.merge(environmentConfig) : _common;

    final violations = _validator.validate(config);
    if (violations.isNotEmpty) {
      throw new ConstraintViolationException(violations);
    }
    return config;
  }

//
//  // TODO: implement environmentNames
//  @override
//  Iterable<String> get environmentNames => null;
}
