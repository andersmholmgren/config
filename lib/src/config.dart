// Copyright (c) 2015, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library config.config;

import 'package:config/src/config_impl.dart';

abstract class Config<C extends Config> {
  C merge(C other);

  Config mergeChildConfigs(Config ours, Config other) {
    if (ours == null) {
      return other;
    } else if (other == null) {
      return ours;
    } else {
      return ours.merge(other);
    }
  }

//  /// override to do any necessary defaulting etc
//  C get finalised => this;
}

abstract class ConfigFactory<C extends Config> {
//  Iterable<String> get environmentNames;

  factory ConfigFactory.standard(C common,
      {C development(), C production()}) = ConfigFactoryImpl.standard;

  factory ConfigFactory.custom(C common,
      C environmentConfigs(String environment)) = ConfigFactoryImpl.custom;

  C configFor(String environmentName, {bool throwIfNoEnvironmentConfig: false});
}

class StandardEnvironmentNames {
  static const String development = 'development';
  static const String production = 'production';
}
