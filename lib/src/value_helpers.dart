// Copyright (c) 2015, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library config.value.helpers;

//import 'package:option/option.dart';
import 'dart:io';
import 'package:converters/converters.dart';

fromEnvironment(String envKey,
    {defaultValue, bool isOptional: false, Type type: String}) {
  final envValue = _fromEnvironment(envKey);

  final rawValue = envValue != null ? envValue : defaultValue;
  final value = convertTo(type, rawValue);

  if (value != null || isOptional) {
    return value;
  }
  throw new ArgumentError("failed to initialse config value. "
      "Environment key: $envKey");
}

String _fromEnvironment(String key) {
  final String systemProperty = new String.fromEnvironment(key);
  if (systemProperty != null) {
    return systemProperty;
  }

  return Platform.environment[key];
}
