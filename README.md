# config

Support for environment configurations

## Usage

A simple usage example:

    import 'package:config/config.dart';

    main() {
      // TODO: ...
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
